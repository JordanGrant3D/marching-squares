//
//  GameScene.swift
//  MarchingSquares
//
//  Created by Jordan Grant on 2024/04/18.
//

import GameplayKit
import SpriteKit
import simd

// MARK: Data for Nodes
struct Vector2: Hashable {
    var x: Int16
    var y: Int16
    
    init(x: Int16, y: Int16) {
        self.x = x
        self.y = y
    }
}


struct NodeData {
    var caseID: Int8
    var node: SKNode
    
    init(caseID: Int8, node: SKNode) {
        self.caseID = caseID
        self.node = node
    }
}

class GameScene: SKScene 
{
    // MARK: Variable Initilisation
    var voxelSize: Double = 20.0
    
    var offset = CGPoint(x: 0, y: 0)
    
    var worldSize: simd_int2 = simd_int2(100, 100)
    
    var isDragging = false
    
    var vertexGrid: [[UInt8]] = []
    
    var objectGrid: [Vector2: NodeData] = [:]
    
    var settingValue: UInt8 = 0
    
    var colorMap: [NSColor] = [NSColor.gray, NSColor.brown, NSColor.blue, NSColor.orange]
    
    var brushSize = 1

    // MARK: Drawing And Getting Input
    override func mouseDown(with event: NSEvent)
    {
        isDragging = true
        updateGrid(with: event)
    }
    
    override func mouseDragged(with event: NSEvent) 
    { 
        if isDragging == true
        {
            updateGrid(with: event)
        }
    }
    
    override func mouseUp(with event: NSEvent) 
    {
        isDragging = false
    }
    
    func updateGrid(with event: NSEvent) {
        let location = event.location(in: self)
        
        let x: Int
        let y: Int
        (x, y) = positionToGridPosition(location: location)

        if x >= 0 && x < worldSize.x && y >= 0 && y < worldSize.y {
            for xOffset in 0..<brushSize {
                for yOffset in 0..<brushSize {
                    trySetBlockValueAt(x: x + xOffset, y: y + yOffset, value: settingValue)
                }
            }
            buildMap()
        }
    }
    
    
    override func keyDown(with event: NSEvent) {
        if let key = event.charactersIgnoringModifiers?.lowercased() {
            if(key == "q")
            {
                settingValue += 1
                if settingValue == colorMap.count + 1 
                { 
                    settingValue = 0
                }
            }
            else if key == "1"
            {
                brushSize = 1
            } 
            else if key == "2"
            {
                brushSize = 2
            } 
            else if key == "3"
            {
                brushSize = 3
            } 
            else if key == "4"
            {
                brushSize = 4
            }
        }
    }
    
    // MARK: Helper Functions
    func positionToGridPosition(location: CGPoint) -> (Int, Int) 
    {
        let positionOffset = CGPoint(x: -1000, y: -760)
        let sceneWidth = scene!.size.width
        let sceneHeight = scene!.size.height
        let x = Int((((location.x - offset.x) + positionOffset.x) + sceneWidth) / voxelSize)
        let y = Int((((location.y - offset.y) + positionOffset.y) + sceneHeight) / voxelSize)
        return (x, y)
    }
    
    func trySetBlockValueAt(x: Int, y: Int, value: UInt8)
    {
        if x >= 0 && y >= 0 && x < vertexGrid.count && y < vertexGrid[0].count {
            vertexGrid[x][y] = value
        }
    }
    
    func addShapeFromPoints(points: [CGPoint], x: Int, y: Int, color: NSColor, caseID: Int8)
    {
        if objectGrid[Vector2(x: Int16(x), y: Int16(y))] != nil {
            let obj = objectGrid[Vector2(x: Int16(x), y: Int16(y))]
            if obj!.caseID != caseID {
                obj!.node.removeFromParent()
                objectGrid[Vector2(x: Int16(x), y: Int16(y))] = nil
            } else {
                return
            }
        }
        
        if points.isEmpty == true
        {
            return
        }
        
        
        var shapePoints = points
        
        shapePoints.append(points[0])
        
        let shapeNode = SKShapeNode(points: &shapePoints, count: shapePoints.count)
        
        shapeNode.position = CGPoint(x: (Double(y) * voxelSize) + offset.x, y: (Double(x) * voxelSize) + offset.y)
        
        addChild(shapeNode)
        
        shapeNode.fillColor = color
        
//        shapeNode.strokeColor = .clear
        
        objectGrid[Vector2(x: Int16(x), y: Int16(y))] = NodeData(caseID: caseID, node: shapeNode)
    }
    
    func getCaseIndexAtPos(x: Int, y: Int) -> Int 
    {
        let topLeft: Bool = getValueAtGridPosition(x: x, y: y + 1) != 0
        let topRight: Bool = getValueAtGridPosition(x: x + 1, y: y + 1) != 0
        let bottomLeft: Bool = getValueAtGridPosition(x: x, y: y) != 0
        let bottomRight: Bool = getValueAtGridPosition(x: x + 1, y: y) != 0
        
        return Int("\(topLeft ? 1 : 0)\(topRight ? 1 : 0)\(bottomRight ? 1 : 0)\(bottomLeft ? 1 : 0)", radix: 2)!
    }
    
    func getValueAtGridPosition(x: Int, y: Int) -> UInt8
    {
        if x < 1 || y < 1 || x >= vertexGrid.count || y >= vertexGrid[0].count
        {
            return 0
        }
        return vertexGrid[x][y]
    }
    
    // MARK: Initilisation Functions

    override func sceneDidLoad()
    {
        offset = CGPoint(x: -(scene!.size.width / 2), y: -(scene!.size.height / 2))
        
        initiliseMap()
        buildMap()
    }
    
    func buildMap() 
    {
        for x in 0..<vertexGrid.count {
            for y in 0..<vertexGrid[0].count {
                let index = getCaseIndexAtPos(x: x, y: y)
                let points = getCase(value: index)
                let colorIndex = getValueAtGridPosition(x: x, y: y)
                var color: NSColor
                if colorIndex == 0 {
                    color = colorMap.first!
                } 
                else
                {
                    color = colorMap[Int(colorIndex) - 1]
                }
                addShapeFromPoints(points: points, x: y, y: x, color: color, caseID: Int8(index))
            }
        }
    }
    
    
    func initiliseMap() 
    {
        for _ in 0..<(worldSize.x / 2) {
            var items: [UInt8] = []
            for _ in 0..<(worldSize.y / 2) {
                let value: UInt8 = 2
                                         
                items.append(value)
            }
            vertexGrid.append(items)
        }
    }
    
    // MARK: Getting Case Values
    
    func getCase(value: Int) -> [CGPoint] 
    {
        switch value {
        case 1:
            return [
                CGPoint(x: 0, y: 0), CGPoint(x: 0, y: voxelSize / 2),
                CGPoint(x: voxelSize / 2, y: 0),
            ]
        case 2:
            return [
                CGPoint(x: voxelSize / 2, y: 0), CGPoint(x: voxelSize, y: 0),
                CGPoint(x: voxelSize, y: voxelSize / 2),
            ]
        case 3:
            return [
                CGPoint(x: 0, y: 0), CGPoint(x: 0, y: voxelSize / 2),
                CGPoint(x: voxelSize, y: voxelSize / 2), CGPoint(x: voxelSize, y: 0),
            ]
        case 4:
            return [
                CGPoint(x: voxelSize / 2, y: voxelSize), CGPoint(x: voxelSize, y: voxelSize),
                CGPoint(x: voxelSize, y: voxelSize / 2),
            ]
        case 5:
            return [
                CGPoint(x: 0, y: 0), CGPoint(x: 0, y: voxelSize / 2),
                CGPoint(x: voxelSize / 2, y: voxelSize), CGPoint(x: voxelSize, y: voxelSize),
                CGPoint(x: voxelSize, y: voxelSize / 2), CGPoint(x: voxelSize / 2, y: 0),
            ]
        case 6:
            return [
                CGPoint(x: voxelSize / 2, y: 0), CGPoint(x: voxelSize / 2, y: voxelSize),
                CGPoint(x: voxelSize, y: voxelSize), CGPoint(x: voxelSize, y: 0),
            ]
        case 7:
            return [
                CGPoint(x: 0, y: 0), CGPoint(x: 0, y: voxelSize / 2),
                CGPoint(x: voxelSize / 2, y: voxelSize), CGPoint(x: voxelSize, y: voxelSize),
                CGPoint(x: voxelSize, y: 0),
            ]
        case 8:
            return [
                CGPoint(x: 0, y: voxelSize / 2), CGPoint(x: 0, y: voxelSize),
                CGPoint(x: voxelSize / 2, y: voxelSize),
            ]
        case 9:
            return [
                CGPoint(x: 0, y: 0), CGPoint(x: 0, y: voxelSize),
                CGPoint(x: voxelSize / 2, y: voxelSize), CGPoint(x: voxelSize / 2, y: 0),
            ]
        case 10:
            return [
                CGPoint(x: voxelSize, y: 0), CGPoint(x: voxelSize, y: voxelSize / 2),
                CGPoint(x: voxelSize / 2, y: voxelSize), CGPoint(x: 0, y: voxelSize),
                CGPoint(x: 0, y: voxelSize / 2), CGPoint(x: voxelSize / 2, y: 0),
            ]

        case 11:
            return [
                CGPoint(x: 0, y: 0), CGPoint(x: 0, y: voxelSize),
                CGPoint(x: voxelSize / 2, y: voxelSize), CGPoint(x: voxelSize, y: voxelSize / 2),
                CGPoint(x: voxelSize, y: 0),
            ]
        case 12:
            return [
                CGPoint(x: 0, y: voxelSize / 2), CGPoint(x: 0, y: voxelSize),
                CGPoint(x: voxelSize, y: voxelSize), CGPoint(x: voxelSize, y: voxelSize / 2),
            ]
        case 13:
            return [
                CGPoint(x: 0, y: 0), CGPoint(x: 0, y: voxelSize),
                CGPoint(x: voxelSize, y: voxelSize), CGPoint(x: voxelSize, y: voxelSize / 2),
                CGPoint(x: voxelSize / 2, y: 0),
            ]
        case 14:
            return [
                CGPoint(x: 0, y: voxelSize / 2), CGPoint(x: 0, y: voxelSize),
                CGPoint(x: voxelSize, y: voxelSize), CGPoint(x: voxelSize, y: 0),
                CGPoint(x: voxelSize / 2, y: 0),
            ]
        case 15:
            return [
                CGPoint(x: 0, y: 0), CGPoint(x: 0, y: voxelSize),
                CGPoint(x: voxelSize, y: voxelSize), CGPoint(x: voxelSize, y: 0),
            ]
        default: return []
        }
    }

}
